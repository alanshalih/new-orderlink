let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
.js('resources/assets/js/form.bundle.js', 'public/js')
.sass('resources/assets/sass/milligram/milligram.sass', 'public/css')
.webpackConfig({
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js',
            OrderlinkComponents : path.resolve(__dirname, 'resources/assets/js/OrderlinkComponents/'),
            Components: path.resolve(__dirname, 'resources/assets/js/components/'),
            Constants: path.resolve(__dirname, 'resources/assets/js/constants/'),
            Container: path.resolve(__dirname, 'resources/assets/js/container/'),
            Views: path.resolve(__dirname, 'resources/assets/js/views/'),
            Helpers: path.resolve(__dirname, 'resources/assets/js/helpers/'),
            Themes: path.resolve(__dirname, 'resources/assets/js/themes/'),
            Api: path.resolve(__dirname, 'resources/assets/js/api/')
        },
    }
})
.setPublicPath('public')
.version()
.browserSync('allneworderlink.dev');
