export const note = {
  title: 'Selalu waspada terhadap pihak tidak bertanggung jawab',
  content:{
    pay: 'Jangan lakukan pembayaran dengan nominal yang berbeda dengan yang tertera pada tagihan kamu',
    transfer: 'Jangan lakukan transfer di luar nomor rekening atas nama Bukalapak',
    learn: 'Pelajarin Selanjutnya'
  }
}

export const transfer = {
  limit: '9 jam 48 menit 35 detik',
  money: 'Rp.3.855.236',
  billNumber: 'BL1811GG5E68INV',
}

export const bank = [
  {
    logo: 'https://kacaaindonesia.com/wp-content/uploads/2018/04/Logo-Bank-BCA-JPG.jpg',
    name: 'Bank BCA, Jakarta',
    rekening: '731 025 2527',
  },
  {
    logo: 'https://upload.wikimedia.org/wikipedia/id/f/fa/Bank_Mandiri_logo.svg',
    name: 'Bank Mandiri, Jakarta',
    rekening: '0700 000 899 992',
  },
  {
    logo: 'https://i2.wp.com/psikologi.fisip-unmul.ac.id/main/wp-content/uploads/2016/07/Logo-Bank-BNI-PNG.png',
    name: 'Bank BNI, Jakarta',
    rekening: '023 827 2088',
  },
  {
    logo: 'https://upload.wikimedia.org/wikipedia/commons/6/68/BANK_BRI_logo.svg',
    name: 'Bank BRI, Jakarta',
    rekening: '034 101 000 743 303',
  },
]

export const intruction = [
  "Transfer dapat melalui ATM, SMS / M-Banking, dan E-Banking.",
  "Masukkan nomor rekening OrderLink.",
  "Masukkan jumlah bayar tepat hingga 3 digit terakhir.",
  "Simpan bukti transfer yang kamu dapatkan."
]
