import { ChartConfig } from "Constants/chart-config";

//earned today
export const earnedToday = {
   color: ChartConfig.color.primary,
   label: 'Earn',
   data: [75, 60, 50, 35, 90, 35, 75, 20, 10, 20, 40, 5, 30, 75, 40, 90, 35, 20, 40, 30, 50, 35, 20, 75],
   labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24']
}

export const data = {
  loader: true,
  search: "",
  selected: [],
  items:[],
  headers: [
    { text: 'Name', align:'center', value: 'name'},
    { text: 'Tag/ Label', align:'center', value: 'label'},
    { text: 'Link', align:'center', value: 'link'},
    { text: 'Created', align:'center', value: 'created'},
    { text: 'Original URL', align:'center', value: 'original'}
  ],
  values: [
    {
      value: false,
      name: 'Link halaman penjualan SDA',
      label: 'Work',
      link: 'http://boleh.click/wa/CSIrman',
      created: 'Jun 3, 2017',
      original: 'http://boleh.click/wa/CSIrman'
    },
    {
      value: false,
      name: 'Link halaman penjualan SDA',
      label: 'Work',
      link: 'http://boleh.click/wa/CSIrman',
      created: 'Jun 3, 2017',
      original: 'http://boleh.click/wa/CSIrman'
    },
    {
      value: false,
      name: 'Link halaman penjualan SDA',
      label: 'Work',
      link: 'http://boleh.click/wa/CSIrman',
      created: 'Jun 3, 2017',
      original: 'http://boleh.click/wa/CSIrman'
    },
    {
      value: false,
      name: 'Link halaman penjualan SDA',
      label: 'Work',
      link: 'http://boleh.click/wa/CSIrman',
      created: 'Jun 3, 2017',
      original: 'http://boleh.click/wa/CSIrman'
    },
    {
      value: false,
      name: 'Link halaman penjualan SDA',
      label: 'Work',
      link: 'http://boleh.click/wa/CSIrman',
      created: 'Jun 3, 2017',
      original: 'http://boleh.click/wa/CSIrman'
    }
  ]
}
