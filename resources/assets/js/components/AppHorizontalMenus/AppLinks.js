// Sidebar Routers
export const category1 = [
   {
      action: 'link',
      title: 'message.pixelLink',
      items: [
         { title: 'message.allPixelLink', path: 'all-pixel-link', exact: true },
         { title: 'message.createNew', path: 'create-new-pixel', exact: true},
         { title: 'message.detailPixelLink', path: 'detail-pixel-link', exact: true}
      ]
   },
  
   {
    action: 'zmdi-view-dashboard',
    title: 'message.LinkPages',
    items: [
      { title: 'message.allLinkPages', path: '/link-pages', exact: true},
      { title: 'message.createNew', path: '/link-pages/create', exact: true},
    ]
  },
   {
     action: 'zmdi-view-dashboard',
     title: 'message.rotator',
     items: [
      { title: 'message.allRotator', path: '/rotator', exact: true},
      { title: 'message.createNew', path: '/rotator/create', exact: true},
     ]
   }
];

export const category2 = [
  {
    action: 'zmdi-view-dashboard',
    title: 'message.form',
    items: [
      { title: 'Form Builder', path: '/forms/create', exact: true},
      { title: 'Statistik', path: '/forms', exact: true}
    ]
  }
];
//
// export const category3 = [
//   {
//     action: 'zmdi-view-dashboard',
//     title: 'xx',
//     items: [
//       {title: 'xxx', path: 'Store', exact: true}
//     ]
//   }
// ]
