import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);
const routes = [{
  path:"/",
  component: require("Views/dashboard/newIndex.vue")
},{
  path: "/other-page",
  component: require("Views/OtherPage.vue")
},

{
  path: "/invoicePage",
  component: require('Views/invoicePage/NewIndex.vue')
},

// authentification
{
  path: "/login",
  component: require('Views/auth/Login.vue')
},
{
  path: '/register',
  component: require('Views/auth/Register.vue')
},

// link pages
{
  path: "/link-pages/create",
  component: require("Views/links/link-pages/LinkPageBuilder.vue")
},
{
  path: "/link-pages/edit/:id",
  component: require("Views/links/link-pages/LinkPageBuilder.vue")
},
{
  path: "/link-pages",
  component: require("Views/links/link-pages/AllLinkPages.vue")
},

// rotator
{
  path: "/rotator/create",
  component: require("Views/links/rotator/CreateEditRotator.vue")
},
{
  path: "/rotator/edit/:id",
  component: require("Views/links/rotator/CreateEditRotator.vue")
},
{
  path: "/rotator",
  component: require("Views/links/rotator/AllRotator.vue")
},

// link
{
  path: "/all-pixel-link",
  component: require("Views/links/pixelLink/AllPixelLink.vue")
},{
  path: "/create-new-pixel",
  component: require("Views/links/pixelLink/CreateNewPixel.vue")
},{
  path: "/detail-pixel-link",
  component: require("Views/links/pixelLink/DetailPixelLink.vue")
},{
  path: "/all-order-link",
  component: require("Views/links/orderLink/AllOrderLink.vue")
},{
  path: "/create-new-order",
  component: require("Views/links/orderLink/CreateNewOrder.vue")
},{
  path: "/all-content-link",
  component: require("Views/links/contentLink/AllContentLink.vue")
},{
  path: "/create-new-content",
  component: require("Views/links/contentLink/CreateNewContent.vue")
},{
  path: "/content-template",
  component: require("Views/links/contentLink/ContentTemplate.vue")
},

// forms
{
  path: "/forms",
  component: require("Views/forms/Index.vue")
},{
  path: "/forms/create",
  component: require("Views/forms/FormBuilder.vue")
},{
  path: "/forms/edit/:id",
  component: require("Views/forms/FormBuilder.vue")
},{
  path: "/store",
  component: require("Views/store/Index.vue")
}
];

export default new VueRouter({
  routes
});
