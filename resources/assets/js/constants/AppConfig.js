/**
 * App Config File
 */
export default {
	appLogo: '/images/logo-orderlink.png',                                   // App Logo,
	darkLogo: '/images/logo-orderlink.png',							         // dark logo
	appLogo2: '/images/logo-orderlink.png',                                    // App Logo 2 For Login & Signup Page
	brand: 'Orderlink.in',                                        			         // Brand Name
	copyrightText: 'Orderlink © 2018 All Rights Reserved.',                     // Copyright Text
	enableUserTour: process.env.NODE_ENV === 'production' ? true : false    // Enable User Tour
}
