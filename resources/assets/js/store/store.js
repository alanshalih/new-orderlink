import Vue from 'vue'
import Vuex from 'vuex'

// modules
import settings from './modules/settings';
import linkPages from './modules/LinkPages';
import pixelCodes from './modules/pixelCodes';
Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        settings,
        linkPages,
        pixelCodes
    }
})
