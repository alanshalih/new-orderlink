const data = `<!-- Facebook Pixel Code -->
<script>

 var id_pixel = 'taruh-id-anda-disini'

  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', id_pixel);
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id="+id_pixel+"&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
`

const state = data

// getters
const getters = {
	
}

// actions
const actions = {
	
}

// mutations
const mutations = {
	
}

export default {
	state,
	getters,
	actions,
	mutations
}
