/**
 * Settings Module
 */
import data from "./data";

const state = data

// getters
const getters = {
	
}

// actions
const actions = {
	
}

// mutations
const mutations = {
	
}

export default {
	state,
	getters,
	actions,
	mutations
}
