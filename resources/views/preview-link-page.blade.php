<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Preview of {{$data->title}}</title>
    <link href="https://fonts.googleapis.com/css?family=Heebo:300,400,500,700,800,900" rel="stylesheet">
    <style>
        body {
            font-family: "Heebo", sans-serif !important;
        }

        .draggable.resizable {
            position: absolute
        }

        #template-wrapper {
            width: 320px;
            max-width: 100%;
            height: 580px;
            position: relative;
            margin: 0 auto;

        }

        #container {
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
        }
    </style>

      {!! $data->head_html !!}

</head>

<body>
    <div id="container">

        <div id="template-wrapper">
            {!! $data->generated_html !!}
        </div>
    </div>

      {!! $data->body_html !!}
</body>

</html>