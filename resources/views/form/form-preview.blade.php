<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <link rel="stylesheet" href="{{ mix('css/milligram.css') }}">
    <title>Document</title>
</head>
<body>
<div id="milligram">
        <form-view server_url="{{$server_url}}" id="{{$id}}"></form-view></div>
    <script src="{{ mix('js/form.bundle.js') }}" ></script>

</body>
</html>