<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Master Data</title>
    <meta name="csrf-token">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
    <!-- Latest compiled and minified JavaScript -->

    {{-- <link rel="stylesheet" href="https://bootswatch.com/3/flatly/bootstrap.css"> --}}
    {{-- <link rel="stylesheet" href="https://bootswatch.com/3/flatly/bootstrap.min.css"> --}}

    {{-- <script src="https://bootswatch.com/_vendor/jquery/dist/jquery.min.js"></script> --}}
    {{-- <script src="https://bootswatch.com/_vendor/popper.js/dist/umd/popper.min.js"> </script> --}}
    {{-- <script src="https://bootswatch.com/_vendor/bootstrap/dist/js/bootstrap.min.js"></script> --}}

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>

    @include('invoicePage.navbar')

    <div class="container">

      @include('invoicePage.note-page')

      <div class="row">
        <div class="col-md-9 col-md-offset-2">
          <div class="panel panel-default">

            <div class="bg-gray">

              @include('invoicePage.transfer-page')

              @include('invoicePage.components.intruction')

              @include('invoicePage.components.paying')

              @include('invoicePage.components.button-payment')

            </div>

          </div> <!-- end panel -->
        </div>
      </div> <!-- end row -->
    </div>


    <script>
      $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();
      });
    </script>
  </body>
</html>
