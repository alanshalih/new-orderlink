<div class="bg-gray py-2 ml-4">
  <span class="transfer">Pembayaran dapat dilakukan ke salah satu rekening a/n OrderLink.in Berikut:</span>
</div>
<div class="panel-body mx-3 bg-gray">
  <div class="row">
    <div class="col-md-6 box-bank" align="center">
      <div>
        <img
          src="https://kacaaindonesia.com/wp-content/uploads/2018/04/Logo-Bank-BCA-JPG.jpg"
          class="img-responsive img-bank"
          >
      </div>
      <span>Bank BCA, Jakarta</span> <br>
      <span>731 025 2527</span> <br> <br>
      <span
        class="tooltip-text"
        />
          Salin No. Rek
      </span>
    </div>
    <div class="col-md-6 box-bank" align="center">
      <div>
        <img
          src="https://upload.wikimedia.org/wikipedia/id/f/fa/Bank_Mandiri_logo.svg"
          class="img-responsive img-bank"
          >
      </div> <br>
      <span>Bank Mandiri, Jakarta</span> <br>
      <span>0700 000 899 992</span> <br> <br>
      <span
        class="tooltip-text"
        />
          Salin No. Rek
      </span>
    </div>
    <div class="col-md-6 box-bank" align="center">
      <div>
        <img
          src="https://i2.wp.com/psikologi.fisip-unmul.ac.id/main/wp-content/uploads/2016/07/Logo-Bank-BNI-PNG.png"
          class="img-responsive img-bank"
          >
      </div> <br>
      <span>Bank BNI, Jakarta</span> <br>
      <span>023 827 2088</span> <br> <br>
      <span
        class="tooltip-text"
        />
          Salin No. Rek
      </span>
    </div>
    <div class="col-md-6 box-bank" align="center">
      <div>
        <img
          src="https://upload.wikimedia.org/wikipedia/commons/6/68/BANK_BRI_logo.svg"
          class="img-responsive img-bank"
          >
      </div> <br>
      <span>Bank BRI, Jakarta</span> <br>
      <span>034 101 000 743 303</span> <br> <br>
      <span
        class="tooltip-text"
        />
          Salin No. Rek
      </span>
    </div>
  </div> <!-- end row -->
</div> <!-- end body intruction -->
