<div class="bg-gray py-2 ml-3">
  <span style="font-size:1.1em">
    Pembelianmu dicatat dengan nomor tagihan Pembayaran <span style="color:red">#BL1811GG5E68INV</span>. OrderLink akan melakukan verifikasi otomatis
    paling lama 30 menit setelah kamu melakukan pembayaran. Jika kamu menghadapi kendala mengenai pembayaran, silahkan
    langsung Hubungi OrderLink.
  </span>
</div>
<div class="panel-body bg-gray mx-3">
  <a href="" class="btn btn-lg btn-danger btn-block">Lihat Tagihan Pembayaran</a>
</div>
