<div class="bg-gray py-2 ml-3">
  <span class="transfer">Petunjuk pembayaran melalui transfer</span>
</div>
<div class="panel-body mx-3 intruction bg-white">
  <div class="row">
    <div class="col-md-3">
      <div class="row">
        <div class="col-md-12" align="center">
          Gambar 1
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <ol>
            <li>Transfer dapat melalui ATM, SMS / M-Banking, dan E-Banking.</li>
          </ol>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="row">
        <div class="col-md-12" align="center">
          Gambar 2
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <ol start="2">
            <li>Masukkan nomor rekening OrderLink.</li>
          </ol>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="row">
        <div class="col-md-12" align="center">
          Gambar 3
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <ol start="3">
            <li>Masukkan jumlah bayar tepat hingga 3 digit terakhir.</li>
          </ol>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="row">
        <div class="col-md-12" align="center">
          Gambar 4
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <ol start="4">
            <li>Simpan bukti transfer yang kamu dapatkan.</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</div> <!-- end body intruction -->
