<div class="row">
  <div class="col-md-9 col-md-offset-2">
    <div class="well">
      <div class="row">
        <div class="col-md-2">
          {{-- <div class="panel-body"> --}}
            <h2>Gambar</h2>
          {{-- </div> --}}
        </div>
        <div class="col-md-10">
          <span class="judul subheading">Selalu waspada terhadap pihak tidak bertanggung jawab</span>
          <ul class="my-3">
            <li> <span>Jangan lakukan pembayaran dengan nominal yang berbeda dengan yang tertera pada tagihan kamu</span> </li>
            <li> <span>Jangan lakukan transfer di luar nomor rekening atas nama OrderLink</span> </li>
          </ul>
          <span class="red--text">Pelajarin Selanjutnya</span>
        </div>
      </div>
    </div>
  </div>
</div>
