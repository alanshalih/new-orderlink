<div class="panel-heading">
  <h4> <b>Pembayaran via Transfer</b> </h4>
</div>
<div class="panel-body text-center bg-white">
  <div class="row">
    <div class="col-md-12 time-limit">
      <span>Batas Pembayaran: <b>9 jam 48 menit 35 detik</b> </span>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <span>Jumlah tagihan:</span>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <b class="headline">Rp.3.855.236</b>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12" style="margin-bottom:80px;">
      <span
        type="button"
        class="tooltip-text"
        data-toggle="tooltip"
        data-placement="bottom"
        data-original-title="Transfer tepat hingga 3 digit terakhir agar tidak menghambat proses verifikasi"
        />
          Salin Jumlah
      </span>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <span class="bill-number" >Nomor tagihan:</span>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <h1 class="red--text bill-number-code"> <b>BL1811GG5E68INV</b> </h1>
    </div>
  </div>
</div> <!-- end panel-body -->
