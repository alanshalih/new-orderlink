@extends('auth.app')

@section('content')
  <div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
        @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('status') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
				<form class="login100-form validate-form" method="POST" action="{{route('password.email')}}" aria-label="{{ __('Reset Password') }}">
          @csrf
					<span class="login100-form-title p-b-26">
						Welcome
					</span>

					<span class="login100-form-title p-b-48">
						<img src="{{asset('images/logo-orderlink.png')}}" class="img-responsive" width="200px">
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
						<input class="input100" type="text" name="email">
						<span class="focus-input100" data-placeholder="Email"></span>
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn">
								{{ __('Send Password Reset Link') }}
							</button>
						</div>
					</div>

          <div class="text-center p-t-115">
						<span class="txt1">
							Remember password ?
						</span>

						<a class="txt2" href="{{route('login')}}">
							Sign In
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>


	<div id="dropDownSelect1"></div>
@endsection
