<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkRotator extends Model
{
    //
    protected $fillable = [
        'list_link', 'url', 'title','author_id','api_version','filters','methods'
    ];
}
