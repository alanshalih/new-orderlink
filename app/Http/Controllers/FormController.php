<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Form;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        return Form::where('author_id',$request->user()->id)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();
        $data['author_id'] = $request->user()->id;

        if($request->id){
            Form::where('id',$request->id)->update($data);
            return $data;
        }
        return Form::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        header("Access-Control-Allow-Origin: *");
        
        
        $form =  Form::where('id',$id)->first();
        if($form){
            return $form;
        }else{
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getFormHTML(Request $request, $id)
    {   
        $server_url = env("SERVER_URL", "http://allneworderlink.test/"); 
        
        return view('form.gethtml',compact('server_url','id'));
    }

    public function formPreview(Request $request, $id)
    {   
        $server_url = env("SERVER_URL", "http://allneworderlink.test/"); 
        
        return view('form.form-preview',compact('server_url','id'));
    }
}
