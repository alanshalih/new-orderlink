<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Api;

class ApiServiceController extends Controller
{
    //
    public function getData()
    {
        $api = new Api();
        $ip =  $api->get_user_ip_address();
        return collect(geoip($ip))->first();
    }
}
