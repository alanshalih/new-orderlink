<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userdomain extends Model
{
    //
    protected $fillable = ['domain','author_id','front_page','status'];
}
