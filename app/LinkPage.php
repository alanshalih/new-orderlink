<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkPage extends Model
{
    //
    protected $fillable = [
        'title', 'el_counter', 'elementList','generated_html','head_html','body_html','author_id'
    ];
}
