<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $fillable = [
        'form_array', 'store_location', 'payment_webhook_url','url','id_counter','action_script','title','head_html','body_html','after_submit_action','email_subject_to_leads','email_content_to_leads','cs_lists','form_color','background_color','container_width','banks','author_id'
    ];
}
