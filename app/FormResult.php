<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormResult extends Model
{
    //
   protected $fillable = ['form_id',
   'visitor_id',
   'other',
   'status_transaksi',
   'produk',
   'total_harga',
   'rincian_harga',
   'payment_channel',
   'phone',
   'nama',
   'email',
   'alamat',
   'courier',
   'province',
   'city',
   'subdistrict',
   'postal_code',
   'location_id',
   'ip',
   'ip_city',
   'ip_region',
   'ip_country',
   'ip_continent',
   'ip_isp',
   'ip_org',
   'ip_type',
   'user_agent',
   'browser',
   'os',
   'device',
   'resolution',
   'referrer',
   'page_url',
    'utm_source',
'utm_medium',
'utm_term',
'utm_content',
'utm_campaign',
'visit_before_action'];
}
