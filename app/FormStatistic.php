<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormStatistic extends Model
{
    //
    protected $fillable = [
    'form_id',
    'visitor_id',
    'ip',
    'ip_city',
    'ip_region',
    'ip_country',
    'ip_continent',
    'ip_isp',
    'ip_org',
    'ip_type',
    'user_agent',
    'browser',
    'os',
    'device',
    'resolution',
    'referrer',
    'page_url',
     'utm_source',
 'utm_medium',
 'utm_term',
 'utm_content',
 'utm_campaign'];
}
