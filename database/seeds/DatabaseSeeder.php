<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         // $this->call(PixelsTableSeeder::class);
         // $this->call(GoogleScriptTableSeeder::class);
         $this->call(RoleTableSeeder::class);
         // $this->call(PermissionTableSeeder::class);
         
         
    }
}
