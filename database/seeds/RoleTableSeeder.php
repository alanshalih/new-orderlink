<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //


        DB::table('roles')->insert([
            'name' => 'gold',
            'display_name' => 'Gold Plan',
            'hit_limit' => 100000,
        ]);

        DB::table('roles')->insert([
            'name' => 'platinum',
            'display_name' => 'Platinum Plan',
            'hit_limit' => 200000,
        ]);

        DB::table('roles')->insert([
            'name' => 'diamond',
            'display_name' => 'Diamond Plan',
            'hit_limit' => 500000,
        ]);

        $admin = DB::table('roles')->insert([
            'name' => 'admin',
            'display_name' => 'Super Admin',
            'hit_limit' => 2000000,
        ]);

        DB::table('role_user')->insert([
            'user_id' => 1,
            'role_id' => 4,
            'ends_at' => Carbon::now()->addYears(10)
        ]);

        Redis::set('user.1.Limiter', 2000000);

    }
}
