<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCCampaignCTemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_campaign_c_template', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('c_campaign_id')->unsigned();     
            $table->integer('c_template_id')->unsigned();
            $table->string('event');
            $table->integer('time')->unsigned();
            $table->string('showOn');
            $table->integer('maksShow')->unsigned();
            $table->string('redirectTo');
            $table->foreign('c_campaign_id')->references('id')->on('c_campaigns') ->onDelete('cascade');

            $table->foreign('c_template_id')->references('id')->on('c_templates') ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_campaign_c_template');
    }
}
