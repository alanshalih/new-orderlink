<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewContentlinkCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_contentlink_campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->text('templates');
            $table->string('url');
            $table->string('title')->nullable();
            $table->string('selected_pixels');
            $table->string('selected_adwords');
            $table->string('selected_template');
            $table->integer('author_id')->unsigned();
            $table->foreign('author_id')->references('id')->on('users') ->onDelete('cascade');
            $table->string('alias')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_contentlink_campaigns');
    }
}
