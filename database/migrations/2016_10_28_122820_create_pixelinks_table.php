<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePixelinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pixelinks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->integer('author_id')->unsigned();
            $table->text('url');
            $table->string('alias')->unique();
            $table->integer('pixel_id')->unsigned()->nullable();
            $table->integer('google_id')->unsigned()->nullable();
            $table->integer('hit')->nullable();
            $table->timestamps();

            $table->foreign('pixel_id')->references('id')->on('pixels');
            $table->foreign('google_id')->references('id')->on('googleids');
            $table->foreign('author_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pixelinks');
    }
}
