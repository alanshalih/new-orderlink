<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->increments('id');
            $table->text('form_array')->nullable();
            $table->text('store_location')->nullable();
            $table->string('payment_webhook_url')->nullable();
            $table->string('url')->nullable()->unique();
            $table->integer('id_counter');
            $table->text('action_script')->nullable();
            $table->string('title')->nullable();
            $table->text('head_html')->nullable();
            $table->text('body_html')->nullable();
            $table->text('after_submit_action')->nullable();
            $table->string('form_color')->nullable();
            $table->string('background_color')->nullable();
            $table->string('container_width')->default('660px');
            $table->string('email_subject_to_leads')->nullable();
            $table->text('email_content_to_leads')->nullable();
            $table->text('cs_lists')->nullable();
            $table->text('banks')->nullable();
            $table->integer('author_id')->unsigned();
            $table->foreign('author_id')->references('id')->on('users') ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
    }
}
