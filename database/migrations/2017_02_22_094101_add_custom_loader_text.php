<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomLoaderText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
               Schema::table('pixelinks', function (Blueprint $table) {

            $table->string('loader_text')->nullable();

        });
           Schema::table('orderlinks', function (Blueprint $table) {

            $table->string('loader_text')->nullable();

        });

              Schema::table('c_campaigns', function (Blueprint $table) {

            $table->string('loader_text')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
             Schema::table('pixelinks', function (Blueprint $table) {

                $table->dropColumn('loader_text');

        });
           Schema::table('orderlinks', function (Blueprint $table) {

            $table->dropColumn('loader_text');

        });

              Schema::table('c_campaigns', function (Blueprint $table) {

            $table->dropColumn('loader_text');

        });
    
    }
}
