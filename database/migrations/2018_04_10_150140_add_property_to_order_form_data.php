<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPropertyToOrderFormData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('order_form_datas', function (Blueprint $table) {   
            $table->string('product')->nullable();
            $table->string('name')->nullable();
            $table->string('totalprice')->nullable();
            $table->string('bank')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::table('order_form_datas', function (Blueprint $table) {
            $table->dropColumn('product');
             $table->dropColumn('name');
             $table->dropColumn('totalprice');
             $table->dropColumn('bank');
        });
    }
}
