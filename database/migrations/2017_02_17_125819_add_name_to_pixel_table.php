<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNameToPixelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::table('pixels', function (Blueprint $table) {

            $table->string('name')->nullable();

            $table->boolean('isDefault')->default(false);

        });
         Schema::table('googleids', function (Blueprint $table) {

            $table->string('name')->nullable();

            $table->boolean('isDefault')->default(false);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('pixels', function (Blueprint $table) {
            $table->dropColumn('name');

            $table->dropColumn('isDefault');
        });

         Schema::table('googleids', function (Blueprint $table) {
            $table->dropColumn('name');

            $table->dropColumn('isDefault');
        });
    }
}
