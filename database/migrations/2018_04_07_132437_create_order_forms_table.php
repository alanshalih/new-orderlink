<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_forms', function (Blueprint $table) {
            $table->increments('id');
                 $table->integer('author_id')->unsigned();
            $table->string('title')->nullable();
            $table->string('page_template')->nullable();
            $table->text('cs')->nullable();
             $table->text('email_to_visitor')->nullable();
             $table->text('form_json')->nullable();
             $table->text('form_embed')->nullable();
             $table->text('tracking_script')->nullable();
             $table->text('after_submit')->nullable();
            $table->timestamps();

              $table->foreign('author_id')->references('id')->on('users') ->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_forms');
    }
}
