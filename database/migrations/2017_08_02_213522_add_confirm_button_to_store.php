<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConfirmButtonToStore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('stores', function (Blueprint $table) {
            $table->boolean('payment_confirm_button')->default(false);
            $table->boolean('show_email_in_checkout')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('stores', function (Blueprint $table) {
        $table->dropColumn('payment_confirm_button');
         $table->dropColumn('show_email_in_checkout');
    });
    }
}
