<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DebugingPixel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        //
        Schema::table('orderlinks', function (Blueprint $table) {
        $table->boolean('debug')->default(false);
        });

        Schema::table('pixelinks', function (Blueprint $table) {
        $table->boolean('debug')->default(false);
        });

        Schema::table('c_campaigns', function (Blueprint $table) {
        $table->boolean('debug')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('orderlinks', function (Blueprint $table) {
        $table->dropColumn('debug');
        });

        Schema::table('pixelinks', function (Blueprint $table) {
        $table->dropColumn('debug');
        });

        Schema::table('c_campaigns', function (Blueprint $table) {
         $table->dropColumn('debug');
        });
    }
}
