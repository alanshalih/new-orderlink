<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportInboxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_inboxes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('body_html');
            $table->text('body_text');
            $table->string('from_email');
            $table->string('from_name');
            $table->string('subject');
            $table->string('to_list');
            $table->boolean('isRead')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_inboxes');
    }
}
