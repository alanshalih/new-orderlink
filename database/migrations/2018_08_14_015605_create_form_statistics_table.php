<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('visitor_id');
            $table->string('ip')->nullable();
            $table->string('ip_city')->nullable();
            $table->string('ip_region')->nullable();
            $table->string('ip_country')->nullable();
            $table->string('ip_continent')->nullable();
            $table->string('ip_isp')->nullable();
            $table->string('ip_org')->nullable();
            $table->string('ip_type')->nullable();
            $table->string('referrer')->nullable();
            $table->string('page_url')->nullable();
            $table->string('utm_source')->nullable();
            $table->string('utm_medium')->nullable();
            $table->string('utm_term')->nullable();
            $table->string('utm_content')->nullable();
            $table->string('utm_campaign')->nullable();
            $table->string('user_agent')->nullable();
            $table->string('os')->nullable();
            $table->string('device')->nullable();
            $table->string('resolution')->nullable();
            $table->integer('form_id')->unsigned(); 
            $table->foreign('form_id')->references('id')->on('forms') ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_statistics');
    }
}
