<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRotatorMethodsToRotatorLink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('link_rotators', function (Blueprint $table) {
            $table->string('methods')->default('Random');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('link_rotators', function (Blueprint $table) {
            $table->dropColumn('methods');
        });
    }
}
