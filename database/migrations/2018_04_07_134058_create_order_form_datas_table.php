<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderFormDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_form_datas', function (Blueprint $table) {
                      $table->increments('id');
                 $table->integer('author_id')->unsigned();
                   $table->integer('order_form_id')->unsigned();
                    $table->integer('order_form_impression_id')->unsigned()->nullable();

            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('label')->nullable();
            $table->text('result')->nullable();
            $table->timestamps();

              $table->foreign('author_id')->references('id')->on('users') ->onDelete('cascade');
              $table->foreign('order_form_id')->references('id')->on('order_forms') ->onDelete('cascade');
                $table->foreign('order_form_impression_id')->references('id')->on('order_form_impressions') ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_form_datas');
    }
}
