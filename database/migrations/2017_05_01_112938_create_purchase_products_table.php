<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('buyer_id')->unsigned();
            $table->string('status')->default('checkout');
            $table->string('products');
            $table->integer('sell_price');
            $table->string('component_price');
            $table->string('courier')->nullable();
            $table->string('courier_package')->nullable();
            $table->string('noresi')->nullable();
            $table->string('pay_with');
            $table->integer('store_id')->unsigned();
            $table->datetime('expired')->nullable();
            $table->timestamps();

             $table->foreign('buyer_id')->references('id')->on('user_customers')->onDelete('cascade');

             $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_products');
    }
}
