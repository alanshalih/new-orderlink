<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderFormImpressionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_form_impressions', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('order_form_id')->unsigned();

            $table->string('ip')->nullable();
            $table->string('user_agent')->nullable();
            $table->string('browser')->nullable();
            $table->string('os')->nullable();
            $table->string('device')->nullable();
            $table->string('resolution')->nullable();
            $table->integer('touch_point')->nullable();
            $table->string('referrer')->nullable();
            $table->string('visitor_id')->nullable();
            $table->string('country')->nullable();
            $table->string('region')->nullable();
            $table->string('city')->nullable();
            $table->string('isp')->nullable();
            $table->boolean('click_button')->default(false);
            $table->foreign('order_form_id')->references('id')->on('order_forms') ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_form_impressions');
    }
}
