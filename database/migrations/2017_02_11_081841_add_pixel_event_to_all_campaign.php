<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPixelEventToAllCampaign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
           Schema::table('pixelinks', function (Blueprint $table) {

            $table->string('pixel_event')->nullable();

        });
           Schema::table('orderlinks', function (Blueprint $table) {

            $table->string('pixel_event')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
