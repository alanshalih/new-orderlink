<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRedirectTimeToOrderlinkAndLander extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
      Schema::table('orderlinks', function (Blueprint $table) {
        $table->integer('redirect_time')->default(5000);
        $table->string('lander')->default('wav2');

    });
      Schema::table('pixelinks', function (Blueprint $table) {
        $table->integer('redirect_time')->default(5000);
    });
  }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
       Schema::table('orderlinks', function (Blueprint $table) {
        $table->dropColumn('redirect_time');
        $table->dropColumn('lander');
    });
       Schema::table('pixelinks', function (Blueprint $table) {
        $table->dropColumn('redirect_time');
    });
   }
}
