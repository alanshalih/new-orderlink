<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLandingPageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('landing_pages', function (Blueprint $table) {
            $table->string('description')->nullable();
            $table->string('thumbnail')->nullable();
            $table->text('head')->nullable();
            $table->text('body')->nullable();
            $table->text('content')->nullable();
            $table->string('url')->unique();

        });


               }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
