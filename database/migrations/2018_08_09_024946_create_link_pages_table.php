<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->integer('author_id')->unsigned();
            $table->integer('el_counter');
            $table->text('elementList');
            $table->text('generated_html');
            $table->text('head_html')->nullable();;
            $table->text('body_html')->nullable();;
            $table->foreign('author_id')->references('id')->on('users') ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link_pages');
        
    }
}
