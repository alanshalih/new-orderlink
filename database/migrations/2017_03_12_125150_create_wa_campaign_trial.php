<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaCampaignTrial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('wacampaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone')->unique();
            $table->text('message')->nullable();
            $table->string('email')->nullable();
            $table->string('name')->nullable();
            $table->integer('upline')->nullable()->unsigned();
            $table->boolean('emailverified')->default(false);
            $table->integer('limit_klik')->default(50);
             $table->string('token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
          Schema::drop('wacampaigns');
    }
}
