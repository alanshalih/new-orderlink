<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPropertyToOrderForm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('order_forms', function (Blueprint $table) {
           
            $table->string('url')->nullable();
            $table->text('follow_up_text')->nullable();
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('order_forms', function (Blueprint $table) {
            $table->dropColumn('url');
             $table->dropColumn('follow_up_text');
        });
    }
}
