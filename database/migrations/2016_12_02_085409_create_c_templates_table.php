<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_templates', function (Blueprint $table) {
                        $table->increments('id');
            $table->integer('author_id')->unsigned();
            $table->text('template')->nullable();
            $table->text('data')->nullable();
            $table->string('cover')->nullable();
            $table->string('title')->nullable();
            $table->boolean('data_backdrop')->default(true);
            $table->string('modalId')->nullable();
            $table->text('modalClass')->nullable();
            $table->timestamps();
            $table->foreign('author_id')->references('id')->on('users') ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('c_templates');
    }
}
