<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdwordsToOrderlink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('orderlinks', function (Blueprint $table) {
        $table->string('adwords')->nullable();
        });

        Schema::table('pixelinks', function (Blueprint $table) {
        $table->string('adwords')->nullable();
        });

        Schema::table('c_campaigns', function (Blueprint $table) {
        $table->string('adwords')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('orderlinks', function (Blueprint $table) {
        $table->dropColumn('adwords');
        });

        Schema::table('pixelinks', function (Blueprint $table) {
        $table->dropColumn('adwords');
        });

        Schema::table('c_campaigns', function (Blueprint $table) {
         $table->dropColumn('adwords');
        });
    }
}
