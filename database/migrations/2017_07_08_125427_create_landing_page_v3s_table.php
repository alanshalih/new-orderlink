<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandingPageV3sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing_page_v3s', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('template_json');
            $table->longText('template_html');
            $table->text('head')->nullable();
            $table->text('body')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('url')->nullable()->index();
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->integer('author_id')->unsigned();

            $table->timestamps();

            $table->foreign('author_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landing_page_v3s');
    }
}
