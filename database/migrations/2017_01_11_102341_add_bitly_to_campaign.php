<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBitlyToCampaign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
      Schema::table('users', function (Blueprint $table) {
        $table->string('bitly_access_token')->nullable();

    });
      Schema::table('pixelinks', function (Blueprint $table) {
        $table->string('bitly')->nullable();

    });
      Schema::table('orderlinks', function (Blueprint $table) {
        $table->string('bitly')->nullable();

    });
      Schema::table('c_campaigns', function (Blueprint $table) {
        $table->string('bitly')->nullable();

    });
  }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
