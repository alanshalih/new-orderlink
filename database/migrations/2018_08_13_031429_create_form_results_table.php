<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    
   
    
   
    
   
    
    
    
    
    public function up()
    {

        
        Schema::create('form_results', function (Blueprint $table) {
            $table->increments('id');
   
            $table->string('visitor_id');
            $table->text('other')->nullable();
            $table->string('status_transaksi')->index()->nullable();
            $table->text('produk')->nullable();
            $table->integer('total_harga')->index()->nullable();
            $table->text('rincian_harga')->nullable();
            $table->string('courier')->nullable();
            $table->string('payment_channel')->nullable();
            $table->string('nama')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('alamat')->nullable();
            $table->string('province')->nullable();
            $table->string('city')->nullable();
            $table->string('subdistrict')->nullable();
            $table->string('postal_code')->nullable();
            $table->integer('location_id')->nullable();
            $table->string('ip')->nullable();
            $table->string('ip_city')->nullable();
            $table->string('ip_region')->nullable();
            $table->string('ip_country')->nullable();
            $table->string('ip_continent')->nullable();
            $table->string('ip_isp')->nullable();
            $table->string('ip_org')->nullable();
            $table->string('ip_type')->nullable();
            $table->string('referrer')->nullable();
            $table->string('page_url')->nullable();
            $table->string('utm_source')->nullable();
            $table->string('utm_medium')->nullable();
            $table->string('utm_term')->nullable();
            $table->string('utm_content')->nullable();
            $table->string('utm_campaign')->nullable();
            $table->string('user_agent')->nullable();
            $table->string('os')->nullable();
            $table->string('device')->nullable();
            $table->string('resolution')->nullable();
            $table->integer('form_id')->unsigned(); 
            $table->integer('visit_before_action')->nullable();
            $table->foreign('form_id')->references('id')->on('forms') ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_results');
    }
}
