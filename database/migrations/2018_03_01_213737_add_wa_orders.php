<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWaOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
           Schema::table('stores', function (Blueprint $table) {
            $table->text('wa_admin')->nullable();
            $table->text('wa_order_text')->nullable();
            $table->string('wa_redirect_url')->nullable();
            $table->string('wa_redirect_rule')->nullable();
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
           Schema::table('stores', function (Blueprint $table) {
            $table->dropColumn('wa_admin');
            $table->dropColumn('wa_order_text');
            $table->dropColumn('redirect_url_after_wa_checkout');
        });
    }
}
