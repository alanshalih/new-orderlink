<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPropertyToOrderFormData2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('order_form_datas', function (Blueprint $table) {   
            $table->string('sub_district')->nullable();
            $table->string('address')->nullable();
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::table('order_form_datas', function (Blueprint $table) {
            $table->dropColumn('sub_district');
             $table->dropColumn('address');
        });
    }
}
