<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTrackingStat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pixelink_stats', function (Blueprint $table) {
            $table->string('ip')->nullable()->change();
            $table->string('user_agent')->nullable()->change();
            $table->string('platform')->nullable()->change();
            $table->string('browser')->nullable()->change();
        });

        Schema::table('orderlink_stats', function (Blueprint $table) {
            $table->string('ip')->nullable()->change();
            $table->string('user_agent')->nullable()->change();
            $table->string('platform')->nullable()->change();
            $table->string('browser')->nullable()->change();
        });

        Schema::table('c_campaign_stats', function (Blueprint $table) {
            $table->string('ip')->nullable()->change();
            $table->string('user_agent')->nullable()->change();
            $table->string('platform')->nullable()->change();
            $table->string('browser')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
