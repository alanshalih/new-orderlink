<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderlinkPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderlink_purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('buyer_id')->unsigned();
            $table->integer('affiliate_id')->unsigned()->nullable();
            $table->string('status')->default('menunggu pembayaran');
            $table->integer('product_id')->unsigned();
            $table->integer('price');
            $table->string('pay_with')->nullable();
            $table->integer('reminder_number')->default(0);
            $table->string('affiliate_payment')->nullable();

            $table->datetime('expired')->nullable();

            $table->foreign('buyer_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('affiliate_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('orderlink_products')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderlink_purchases');
    }
}
