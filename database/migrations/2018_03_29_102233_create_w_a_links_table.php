<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWALinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('w_a_links', function (Blueprint $table) {
            $table->increments('id');
            $table->text('cs');
            $table->string('title')->nullable();
            $table->string('answer')->nullable();
            $table->string('pixels')->nullable();
            $table->string('event')->nullable();
            $table->string('adwords')->nullable();
            $table->string('protocol');
            $table->string('alias')->unique()->index();
            $table->integer('author_id')->unsigned();
            $table->integer('hit')->nullable();
            $table->string('view_id');
            $table->timestamps();




               $table->foreign('author_id')->references('id')->on('users') ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('w_a_links');
    }
}
