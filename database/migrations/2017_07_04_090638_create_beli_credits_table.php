<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeliCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beli_credits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sms_credit')->default(0);
            $table->integer('email_credit')->default(0);
            $table->integer('price')->default(0)->index();
            $table->integer('user')->unsigned();
            $table->string('status')->default('pending');
            $table->datetime('expired')->nullable();
            $table->timestamps();

            $table->foreign('user')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beli_credits');
    }
}
