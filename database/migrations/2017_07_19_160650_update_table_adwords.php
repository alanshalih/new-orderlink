<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableAdwords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pixelinks', function (Blueprint $table) {
          $table->dropForeign('pixelinks_google_id_foreign');
        });

        Schema::table('orderlinks', function (Blueprint $table) {
          $table->dropForeign('orderlinks_google_id_foreign');
        });

        Schema::table('c_campaigns', function (Blueprint $table) {
          $table->dropForeign('c_campaigns_google_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
