<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('store_id')->unsigned();
            $table->string('title');
            $table->string('thumbnail')->nullable();
            $table->string('short_description')->nullable();
            $table->text('long_description')->nullable();
            $table->integer('price');
            $table->boolean('show_in_marketplace')->default(false);
            $table->boolean('free_ongkir')->default(false);
            $table->string('province')->nullable();
            $table->string('city')->nullable();
            $table->integer('weight')->default(1000);
            $table->timestamps();
             $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
