<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLoadTrackingScriptAndRequireMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('order_forms', function (Blueprint $table) {
           
            $table->string('require_message')->default('dibutuhkan')->nullable();
            $table->text('after_submit_tracking_script')->nullable();
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
           Schema::table('order_forms', function (Blueprint $table) {
            $table->dropColumn('require_message');
             $table->dropColumn('after_submit_tracking_script');
        });
    }
}
