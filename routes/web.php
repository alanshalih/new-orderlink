<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/invoice-page',function(){
  return view('invoicePage/index');
});

Route::get('/api-service','ApiServiceController@getData');


Route::get('/forms/{id}','FormController@show');

Route::group(['middleware' => ['cors']], function () {

    Route::post('/form-result','FormResultController@store');

    Route::post('/form-statistic','FormStatisticsController@store');




});

Route::group(['middleware' => ['auth']], function () {

    Route::resource('/link-pages','LinkPagesController');

    Route::resource('/link-rotator','LinkRotatorController');

    Route::resource('/my-domain','UserDomainController');

    Route::resource('/forms','FormController');



    Route::get('/form-html/{id}', 'FormController@getFormHTML');

    Route::get('/form/preview/{id}', 'FormController@formPreview');

});

Route::get('/form-view', function () {
    return view('form-view');
});


Route::get('/home', 'HomeController@index')->name('home');
